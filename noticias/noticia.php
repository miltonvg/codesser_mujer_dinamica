<?php
session_start();
ini_set('date.timezone', 'America/Buenos_Aires');
// echo phpinfo();
include("../funciones/setup.php"); include("../funciones/fecha.php");


$gbd = conecta();



function gd($dato){ // get dato
  if(isset($_REQUEST[$dato]) and ($_REQUEST[$dato]!=NULL or $_REQUEST[$dato]!="" )){
    return $_REQUEST[$dato]; 
  }
  return NULL;
}


function gsql($sql,$datos){ // get sql
  global $gbd;
  try{
    $sentencia = $gbd->prepare($sql);

    if(count($datos) == 0 or $datos == NULL){
      $sentencia->execute();
    }else{
      $sentencia->execute($datos);
    }
    return $sentencia;   
  }catch(Exception $e){
    echo "Error al generar la consulta";
  }
}

$id_not = gd('not');
$array = array($id_not);
$sql_noticias = "SELECT * FROM NOTICIA WHERE ID_NOT = ?";
$result_noticias = gsql($sql_noticias, $array);
$datos_noticias = $result_noticias -> fetch(PDO::FETCH_ASSOC);
?>





<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
		<meta name="viewport" content="width=device-width, initial-scale=1"> 
		<title></title>
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<meta name="author" content="" />
		<link rel="shortcut icon" href="../favicon.ico">
		<link rel="stylesheet" type="text/css" href="css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="css/main-news.css" />
		<link rel="stylesheet" type="text/css" href="css/component.css" />
		<!--[if IE]>
  		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
	<body class="demo-3">
		<div id="container" class="container intro-effect-sliced">
			<header class="header">
				<div class="bg-img"><img src="../Admin/noticias/portadas/<?php echo $datos_noticias['IMAGEN_NOT']?>" alt="Background Image" /></div>
				<div class="title">			
					<div class="breadcrumbs">
						<div class="breadcrumbs-inner">					
							<span>
								<span><a href="../index.php#cd-placeholder-6">Inicio</a></span>
								<!-- <span><a class="border-right" href="#">Noticias</a></span> -->
							</span>
						</div>
					</div>				
					<h1><?php echo $datos_noticias['NOMBRE_NOT']?></h1>
					<p class="subline"><?php echo $datos_noticias['CABECERA_NOT']?></p>
					<p>por <strong><?php echo $datos_noticias['USUARIO_NOT']?></strong> &#8212; Publicado en <strong>La Serena</strong> el <strong><?php echo entrega_fecha2($datos_noticias['FECHA_INGRESO_NOT'])?></strong></p>
				</div>
				<div class="bg-img"><img src="../Admin/noticias/portadas/<?php echo $datos_noticias['IMAGEN_NOT']?>" alt="Background Image" /></div>
			</header>
			<button class="trigger" data-info="Haga clic para ver la noticia completa"><span>Trigger</span></button>
			<article class="content">
				<div>
				<?php 
					$nombre_archivo ="../Admin/noticias/".$id_not.".txt";
					$handle = fopen($nombre_archivo, "r");
					$descripcion_not = fread($handle, filesize($nombre_archivo));
					$descripcion_not = stripslashes($descripcion_not);
					fclose($handle);
				?>
					<?php echo $descripcion_not;?>
				</div>
			</article>
		</div><!-- /container -->
		<script src="js/classie.js"></script>
		<script>
			(function() {

				// detect if IE : from http://stackoverflow.com/a/16657946		
				var ie = (function(){
					var undef,rv = -1; // Return value assumes failure.
					var ua = window.navigator.userAgent;
					var msie = ua.indexOf('MSIE ');
					var trident = ua.indexOf('Trident/');

					if (msie > 0) {
						// IE 10 or older => return version number
						rv = parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
					} else if (trident > 0) {
						// IE 11 (or newer) => return version number
						var rvNum = ua.indexOf('rv:');
						rv = parseInt(ua.substring(rvNum + 3, ua.indexOf('.', rvNum)), 10);
					}

					return ((rv > -1) ? rv : undef);
				}());


				// disable/enable scroll (mousewheel and keys) from http://stackoverflow.com/a/4770179					
				// left: 37, up: 38, right: 39, down: 40,
				// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
				var keys = [32, 37, 38, 39, 40], wheelIter = 0;

				function preventDefault(e) {
					e = e || window.event;
					if (e.preventDefault)
					e.preventDefault();
					e.returnValue = false;  
				}

				function keydown(e) {
					for (var i = keys.length; i--;) {
						if (e.keyCode === keys[i]) {
							preventDefault(e);
							return;
						}
					}
				}

				function touchmove(e) {
					preventDefault(e);
				}

				function wheel(e) {
					// for IE 
					//if( ie ) {
						//preventDefault(e);
					//}
				}

				function disable_scroll() {
					window.onmousewheel = document.onmousewheel = wheel;
					document.onkeydown = keydown;
					document.body.ontouchmove = touchmove;
				}

				function enable_scroll() {
					window.onmousewheel = document.onmousewheel = document.onkeydown = document.body.ontouchmove = null;  
				}

				var docElem = window.document.documentElement,
					scrollVal,
					isRevealed, 
					noscroll, 
					isAnimating,
					container = document.getElementById( 'container' ),
					trigger = container.querySelector( 'button.trigger' );

				function scrollY() {
					return window.pageYOffset || docElem.scrollTop;
				}
				
				function scrollPage() {
					scrollVal = scrollY();
					
					if( noscroll && !ie ) {
						if( scrollVal < 0 ) return false;
						// keep it that way
						window.scrollTo( 0, 0 );
					}

					if( classie.has( container, 'notrans' ) ) {
						classie.remove( container, 'notrans' );
						return false;
					}

					if( isAnimating ) {
						return false;
					}
					
					if( scrollVal <= 0 && isRevealed ) {
						toggle(0);
					}
					else if( scrollVal > 0 && !isRevealed ){
						toggle(1);
					}
				}

				function toggle( reveal ) {
					isAnimating = true;
					
					if( reveal ) {
						classie.add( container, 'modify' );
					}
					else {
						noscroll = true;
						disable_scroll();
						classie.remove( container, 'modify' );
					}

					// simulating the end of the transition:
					setTimeout( function() {
						isRevealed = !isRevealed;
						isAnimating = false;
						if( reveal ) {
							noscroll = false;
							enable_scroll();
						}
					}, 600 );
				}

				// refreshing the page...
				var pageScroll = scrollY();
				noscroll = pageScroll === 0;
				
				disable_scroll();
				
				if( pageScroll ) {
					isRevealed = true;
					classie.add( container, 'notrans' );
					classie.add( container, 'modify' );
				}
				
				window.addEventListener( 'scroll', scrollPage );
				trigger.addEventListener( 'click', function() { toggle( 'reveal' ); } );
			})();
		</script>
	</body>
</html>