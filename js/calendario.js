var calendar, calendarDayMonth, calendarMonth, closeAlerts, closeLogin, closeMap, closeMenu, equidistar, loadMap, loaderGifIn, loaderGifOut, loaderIn, loaderOut, months, openAlert, openLogin, openMap, openMenu, openRecover, openRegister, out_time, renderContent, scrollConditions, setupAjaxNavigation, setupElements, switchSite, validarEmail, _in, _out;

months = ["", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

calendar = function(data) {
	var from, from_day, from_format, from_month, from_split, from_year, month, n, to, to_day, to_format, to_month, to_split, to_year, tt, tt_date, tt_date_day, tt_date_format, tt_date_month, tt_date_split, tt_date_year, year, _i, _j, _k, _l, _m, _n, _ref;
	$(".calendar").html('<div class="inner"></div>');
	from = data.events[0].date;
	to = data.events[data.events.length - 1].date;
	from_split = from.split("-");
	from_year = parseInt(from_split[0]);
	from_month = parseInt(from_split[1]);
	from_day = parseInt(from_split[2]);
	from_format = from_year + "-" + from_month + "-" + from_day;
	to_split = to.split("-");
	to_year = parseInt(to_split[0]);
	to_month = parseInt(to_split[1]);
	to_day = parseInt(to_split[2]);
	to_format = to_year + "-" + to_month + "-" + to_day;
	if (data.extramonth) {
		to_month = to_month + data.extramonth;
		if (to_month > 12) {
			to_year = to_year + 1;
			to_month = to_month - 12;
		}
	}
	for (year = _i = from_year; from_year <= to_year ? _i <= to_year : _i >= to_year; year = from_year <= to_year ? ++_i : --_i) {
		if (from_year === to_year) {
			for (month = _j = from_month; from_month <= to_month ? _j <= to_month : _j >= to_month; month = from_month <= to_month ? ++_j : --_j) {
				calendarMonth(year, month, data);
			}
		} else {
			if (year === from_year) {
				for (month = _k = from_month; from_month <= 12 ? _k <= 12 : _k >= 12; month = from_month <= 12 ? ++_k : --_k) {
					calendarMonth(year, month, data);
				}
			}
			if (year !== from_year && year !== to_year) {
				for (month = _l = 1; _l <= 12; month = ++_l) {
					calendarMonth(year, month, data);
				}
			}
			if (year === to_year) {
				for (month = _m = 1; 1 <= to_month ? _m <= to_month : _m >= to_month; month = 1 <= to_month ? ++_m : --_m) {
					calendarMonth(year, month, data);
				}
			}
		}
	}
	if (data.events) {
		for (n = _n = 0, _ref = data.events.length - 1; 0 <= _ref ? _n <= _ref : _n >= _ref; n = 0 <= _ref ? ++_n : --_n) {
			tt = data.events[n];
			tt_date = tt.date;
			tt_date_split = tt_date.split("-");
			tt_date_year = parseInt(tt_date_split[0]);
			tt_date_month = parseInt(tt_date_split[1]);
			tt_date_day = parseInt(tt_date_split[2]);
			tt_date_format = tt_date_year + "-" + tt_date_month + "-" + tt_date_day;
			$(".calendar .day[data-date='" + tt_date_format + "']").addClass("enabled");
			if (!$(".calendar .day[data-date='" + tt_date_format + "'] .calendar-tooltip-content").length) {
				$(".calendar .day[data-date='" + tt_date_format + "'] .innerday").append('<div class="calendar-tooltip"><div class="calendar-tooltip-triangle"></div><div class="calendar-tooltip-content"></div></div>');
			}
			if(tt.photo_url){
				htmlimg = '<div class="calendar-event-thumb"><img src="' + tt.photo_url + '" /></div>';
				classimg = ''
			}else{
				htmlimg = '';
				classimg = 'no-thumb'
			}
			//console.log(tt);
			$(".calendar .day[data-date='" + tt_date_format + "'] .calendar-tooltip-content").append(
				'<div class="calendar-event '+classimg+'" data-type="'+tt.event_type_id+'" data-externo="'+tt.evento_externo+'" data-gratis="'+tt.evento_gratis+'">'+
					'<a href="' + $("body").attr("data-url") + "/actividades/actividad/?e=" + tt.id + '">'+
						htmlimg+
						'<div class="calendar-event-text"><div class="calendar-event-tag" data-tag="' + tt.event_type_id + '"></div> ' + tt.title + '</div>'+
					'</a>'+
				'</div>'
			);

		}
	}
	$(".calendar .inner").append("<div class='calendar-navigation'><button class='nav nav-left'></button> <button class='nav nav-right'></button></div>");
	$(".calendar .month").eq(0).addClass("in-right");
	$(".calendar .calendar-navigation .nav-left").addClass("disabled");
	return $(".calendar .calendar-navigation button").click(function(event) {
		var limit, mo, next, prev, _this;
		if (!$(".calendar").hasClass("animation")) {
			_this = $(this);
			if ($(".calendar .month.in-right").length) {
				mo = $(".calendar .month.in-right");
			}
			if ($(".calendar .month.in-left").length) {
				mo = $(".calendar .month.in-left");
			}
			if (!_this.hasClass("disabled")) {
				prev = mo.index() - 1;
				next = mo.index() + 1;
				limit = $(".calendar .month").length - 1;
				if (_this.hasClass("nav-right")) {
					$(".calendar .calendar-navigation .nav-left").removeClass("disabled");
					if (next === limit) {
						$(".calendar .calendar-navigation .nav-right").addClass("disabled");
					}
				}
				if (_this.hasClass("nav-left")) {
					$(".calendar .calendar-navigation .nav-right").removeClass("disabled");
					if (prev === 0) {
						$(".calendar .calendar-navigation .nav-left").addClass("disabled");
					}
				}
				$(".calendar").addClass("animation");
				if (_this.hasClass("nav-right")) {
					mo.addClass("out-left");
					mo.parent().find(".month").eq(next).addClass("in-left");
				}
				if (_this.hasClass("nav-left")) {
					mo.addClass("out-right");
					mo.parent().find(".month").eq(prev).addClass("in-right");
				}
				setTimeout(function() {
					mo.removeClass("in-right in-left out-right out-left");
					return $(".calendar").removeClass("animation");
				}, 500);
			}
		}
		event.stopPropagation();
		
		return false;
	});
};


calendarMonth = function(year, month, data) {
	var cssclass, d, day, left, _i, _ref, _results;
	$(".calendar .inner").append("<div class='month' data-year='" + year + "' data-month='" + month + "'><h3>" + months[month] + " <span>" + year + "</span></h3></div>");
	$(".calendar .month[data-year=" + year + "][data-month=" + month + "]").append("<span class='day week'>Lun</span><span class='day week'>Mar</span><span class='day week'>Mie</span><span class='day week'>Jue</span><span class='day week'>Vie</span><span class='day week'>Sab</span><span class='day week'>Dom</span>");
	_results = [];
	for (day = _i = 1, _ref = calendarDayMonth(month, year) + 1; 1 <= _ref ? _i < _ref : _i > _ref; day = 1 <= _ref ? ++_i : --_i) {
		cssclass = "day";
		if (day === 1) {
			d = new Date();
			
			d.setYear(year);
			d.setMonth(month - 1);
			d.setDate(day);
			left = d.getDay() - 1;
			if (left === -1) {
				left = 6;
			}
			cssclass += " left" + left;
		}
		_results.push($(".calendar .month[data-year=" + year + "][data-month=" + month + "]").append("<div href='#' data-date='" + year + "-" + month + "-" + day + "' class='" + cssclass + "'><span class='innerday'>" + day + "</span></div> "));
	}
	return _results;
};


calendarDayMonth = function(humanMonth, year) {
	return new Date(year || new Date().getFullYear(), humanMonth, 0).getDate();
};

/* --------------------------------------------
		 Begin scripts.coffee
--------------------------------------------
*/


out_time = false;

_in = function(element) {
	clearTimeout(out_time);
	return element.removeClass("out").addClass("in");
};


_out = function(element) {
	element.addClass("out");
	return out_time = setTimeout(function() {
		return element.removeClass("in out");
	}, 500);
};


loaderIn = function() {
	$("#logo").addClass("animate");
	$("#body").addClass("out");
	return $("html, body").animate({
		scrollTop: 0
	}, 500);
};


loaderOut = function() {
	$("#body").removeClass("out");
	return setTimeout(function() {
		return $("#logo").removeClass("animate");
	}, 1000);
};


loaderGifIn = function(element) {
	element.find(".submit").hide();
	element.append("<div class='loadergif'><div></div></div>");
};


loaderGifOut = function(element) {
	element.find(".submit").show();
	element.find(".loadergif").remove();
};


loaderFormIn = function(element){
	element.append("<div class='loading-form'><div class='loading-form-inner'><div class='loadergif'><div></div></div> Cargando datos</div></div>");
}

loaderFormOut = function(element){
	element.find(".loading-form").remove();	
}


loaderDelete = function(element){
	element.append("<div class='loading-form'><div class='loading-form-inner'><div class='loadergif'><div></div></div> Eliminando datos</div></div>");	
}




renderContent = function() {


	if ($(".layout-cols.layout-cols-50-50").length) {
		$("body").removeClass("layout-cols-100");
		$("body").addClass("layout-cols-50-50");
	}
	if ($(".layout-cols.layout-cols-100").length) {
		$("body").removeClass("layout-cols-50-50");
		$("body").addClass("layout-cols-100");
	}
	if (!$(".layout-cols.layout-cols-50-50").length && !$(".layout-cols.layout-cols-100").length) {
		$("body").removeClass("layout-cols-50-50");
		$("body").removeClass("layout-cols-100");
	}


	
	$(window).scroll(scrollConditions);
	$(window).resize(scrollConditions);


	if ($(".calendar").length && $(".calendar").attr("data-url")) {
		$(".calendar").append("<div class='loadergif'><div></div></div>");
		$.ajax({
			url: $(".calendar").attr("data-url"),
			dataType: "json"
		}).done(function(result) {

			if(result[0]){

				calendar({
					events: result,
					extramonth: 1
				});

				$.ajax({
					url: "http://api-crm.asech.cl/obtenerTiposCalendario.php",
					dataType: "json"
				}).done(function(result) {
					var r, _i, _len, _results;
					_results = [];
					for (_i = 0, _len = result.length; _i < _len; _i++) {
						r = result[_i];
						//console.log(r);
						_results.push($(".calendar-event-tag[data-tag='" + r.key + "']").html("<div>" + r.value + "</div>"));
					}
					return _results;

				});

			}else{
				$(".calendar").html("<div class='nocalendar'>No hay eventos en el calendario</div>");
			}

		});
	}





	$.ajax({
		url: "http://api-crm.asech.cl/obtenerTiposCalendario.php",
		dataType: "json"
	}).done(function(results) {
		//console.log(results);
		$(".calendar-filter ul").html("<li class='selected'><a href='#' data-type='-'>Todas</a></li>");
		$(".calendar-filter ul").append("<li><a href='#' data-type='gratis'>Eventos gratis</a></li>");
		$(".calendar-filter ul").append("<li><a href='#' data-type='externo'>Eventos externos</a></li>");
		$(results).each(function(index,r){
			$(".calendar-filter ul").append("<li><a href='#' data-type='"+r.key+"'>"+r.value+"</a></li>");
		});

		$(".calendar-filter a").live("click",function(){
			filter_type = $(this).attr("data-type");
			//console.log(filter_type);

			$(".calendar-filter li").removeClass("selected");
			$(this).parent().addClass("selected");

			if(filter_type!="-"){

				$(".calendar .calendar-event").addClass("hide");

				if(filter_type=="gratis" || filter_type=="externo"){
					$(".calendar .calendar-event[data-"+filter_type+"='1']").removeClass("hide");
				}else{
					$(".calendar .calendar-event[data-type='"+filter_type+"']").removeClass("hide");					
				}

				$(".calendar .day").removeClass("enabled");
				$(".calendar .day").each(function(){
					if( $(this).find(".calendar-event:not(.hide)").length ){
						$(this).addClass("enabled");
					}
				});
			}else{
				$(".calendar .calendar-event").removeClass("hide");
				$(".calendar .day").each(function(){
					if( $(this).find(".calendar-event").length ){
						$(this).addClass("enabled");
					}
				});
			}

			return false;
		});

	});


};




$(document).ready(function() {
	renderContent();
	
	setupElements();
	
	facebookConnect();
	
	setupAjaxNavigation();
	
	return $(window).resize(function() {
		return equidistar();
	});
});



