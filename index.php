<?php
session_start();
ini_set('date.timezone', 'America/Buenos_Aires');
// echo phpinfo();
include("funciones/setup.php"); include("funciones/fecha.php");

$gbd = conecta();


function gd($dato){ // get dato
  if(isset($_REQUEST[$dato]) and ($_REQUEST[$dato]!=NULL or $_REQUEST[$dato]!="" )){
    return $_REQUEST[$dato]; 
  }
  return NULL;
}


function gsql($sql,$datos){ // get sql
  global $gbd;
  try{
    $sentencia = $gbd->prepare($sql);

    if(count($datos) == 0 or $datos == NULL){
      $sentencia->execute();
    }else{
      $sentencia->execute($datos);
    }
    return $sentencia;   
  }catch(Exception $e){
    echo "Error al generar la consulta";
  }
}
?>
<!doctype html>
<html lang="es" class="no-js">
<head>

<style>
#cd-intro {
  position: relative;
  height: 300px;
  background: url(img/fondos/cover.jpg) no-repeat center center;
  background-size: cover;
  z-index: 2;
}
</style>
	<meta charset="UTF-8">
	<meta charset="ISO-8859-1">
	  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
	  <meta name="Resource-Type" content="document" />	  
	  <meta name="robots" content="index, follow" />    
	  <meta name="description" content="MujerDinámica es un programa de pre-aceleración de emprendimientos liderados por mujeres de la región de Coquimbo">
	  <meta name="author" content="Codesser">	

	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="css/reset.css"> <!-- CSS reset -->
	<link rel="stylesheet" href="css/style.css"> <!-- Resource style -->
	<link rel="stylesheet" type="text/css" href="css/component.css" />
    <link rel="stylesheet" type="text/css" href="css/responsive.css" />
    <link rel="stylesheet" type="text/css" href="css/table/normalize.css" />
	<link rel="stylesheet" type="text/css" href="css/table/demo.css" />
	<link rel="stylesheet" type="text/css" href="css/table/component.css" />
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png"/>


    
    
    <link href="css/ionicons.css" rel="stylesheet" type="text/css" />



    	<script src="js/modernizr.js"></script> <!-- Modernizr -->
	<script src="js/modernizr.custom.js"></script>
    
<!-- CALENDARIO -->
		<link rel="stylesheet" type="text/css" href="js/Calendario/css/calendar.css" />
		<link rel="stylesheet" type="text/css" href="js/Calendario/css/custom_2.css" />
		<script src="js/Calendario/js/modernizr.custom.63321.js"></script>
<!-- FIN -->

<link rel="stylesheet" href="css/ionicons/css/ionicons.min.css">





  	
	<title>StartUp Fase 0 Coquimbo</title>
</head>
<body>
	<header class="cd-header">
		<div id="cd-logo" class="hidden"><a href="#"><img src="img/logo_full.png" alt="Logo "></a></div>
	</header>
	<section id="cd-intro">
		<div id="cd-intro-tagline">
			<h1>MUJERES DINÁMICAS</h1> 
            <h2>¡Atrévete a Innovar y Emprende!</h2>    
            <a href="https://codesser.typeform.com/to/nYLO8v" class="cd-btn" target="_blank">Postula Aquí</a>        
			 <!--<a href="https://codesser.typeform.com/to/nYLO8v" class="cd-btn" target="_blank">Postula Aquí</a>-->
		</div> <!-- #cd-intro-tagline -->
	</section> <!-- #cd-intro -->

	<div class="cd-secondary-nav">
		<a href="#0" class="cd-secondary-nav-trigger">Menu<span></span></a> <!-- button visible on small devices -->
		<nav>
			<ul>
				<li>
					<a href="#cd-placeholder-1">
						<b>¿ Qué es ?</b>
						<span><i class="icon ion-ios-lightbulb-outline"></i> </span><!-- icon -->   
					</a>
				</li>
               	<li>
					<a href="#cd-placeholder-2">
						<b>Proceso</b>
						<span><i class="ion-ios-list-outline"></i> </span><!-- icon -->   
					</a>
				</li>
				<li>
					<a href="#cd-placeholder-3">
						<b>Bases</b>
						<span><i class="ion-ios-download-outline"></i> </span><!-- icon -->   
					</a>
				</li>
              	<li>
					<a href="#cd-placeholder-4">
						<b>Postulación</b>
						<span><i class="ion-ios-bolt-outline"></i> </span><!-- icon -->   
					</a>
				</li>                
				<li>
					<a href="#cd-placeholder-5">						
						<b>Agenda</b>
                        <span><i class="ion-ios-calendar-outline"></i> </span><!-- icon -->                   
					</a>
				</li>
              	<li>
					<a href="#cd-placeholder-6">
						<b>Noticias</b>
						<span><i class="ion-ios-paper-outline"></i> </span><!-- icon -->   
					</a>
				</li>                
				<li>
					<a href="#cd-placeholder-7">
						<b>Staff</b>
						<span><i class="ion-ios-heart-outline"></i> </span><!-- icon -->   
					</a>
				</li>
			</ul>
		</nav>
	</div> <!-- .cd-secondary-nav -->
	<main class="cd-main-content">
		<section id="cd-placeholder-1" class="cd-section cd-container">
			<div class="side-left">
				<h2>¿Qué es?</h2>
				<p style="text-align:justify;">
	<b class="strong">MUJERDINÁMICA</b> es un programa de pre-aceleración de emprendimientos liderados por mujeres de la región de Coquimbo que instalará competencias y habilidades claves en 70 mujeres para desarrollar emprendimientos innovadores con alto potencial de crecimiento (dinámicos), aportando al desarrollo y diversificación de los sectores productivos de la Región de Coquimbo. 
	<!--
	Se levantará una Red de Mujeres con foco en Innovación con el fin de generar asociatividad, colaboración y conexión con otras redes e instituciones.
	Se apoyará en el acceso a financiamiento público y/o privado de Fondos Semilla y de Aceleración a aquellas que cumplan los objetivos del Programa.
		-->

				</p>
				<!--
	            <br>
				<h2>¿Qué vas a aprender?</h2>
				<p style="text-align:justify;">
	Podrás desarrollar habilidades claves que serán transferidas por expertos reconocidos a nivel nacional e internacional, tales como:
				</p>        
	            <br>              
	            <ul>
	                <li>Levantamiento de oportunidades de negocio en base a las necesidades del mercado.</li>
	                <li>Validación de ideas de negocio con clientes reales.</li>
	                <li>Creación de proyectos de emprendimiento.</li>
	                <li>Búsqueda de financiamiento público y privado.</li>                                                
	            </ul>
	            -->
	        </div>
	        <div class="side-right">	       
	        	<img src="img/fondos/girl_desk.png" class="girl-desk-1" alt="Qué es Mujer Dinámica">
	        </div>
		</section> <!-- #cd-placeholder-1 -->

		<section id="cd-placeholder-2" class="cd-section cd-container">
			<h2>Proceso</h2>

                <section id="cd-timeline" class="cd-container">
                <div class="cd-timeline-block">
                    <div class="cd-timeline-img cd-con">
                        <span><i class="ion-ios-flag"></i></span><!-- icon -->   
                    </div> <!-- cd-timeline-img -->
        
                    <div class="cd-timeline-content">
                        <h2>Apertura de postulaciones.</h2>
                        <span><i class="ion-ios-calendar-outline"></i>8 de Oct. al 8 de Nov</span>
                        <p>200 Postulaciones</p>
                    </div> <!-- cd-timeline-content -->
                </div> <!-- cd-timeline-block -->
        
                <div class="cd-timeline-block">
                    <div class="cd-timeline-img cd-mod1">
                        <span><i class="ion-ios-list"></i></span><!-- icon -->   
                    </div> <!-- cd-timeline-img -->
        
                    <div class="cd-timeline-content">
                        <h2>Selección de perfiles.</h2>
                        <span><i class="ion-ios-calendar-outline"></i>13 de Noviembre</span>
                        <p>70 Seleccionadas</p>
                      <!--   <span class="cd-date">8 horas</span> -->
                    </div> <!-- cd-timeline-content -->
                </div> <!-- cd-timeline-block -->
        
                <div class="cd-timeline-block">
                    <div class="cd-timeline-img cd-mod2">
                        <span><i class="ion-ios-pulse-strong"></i></span><!-- icon --> 
                    </div> <!-- cd-timeline-img -->
        
                    <div class="cd-timeline-content">
                        <h2>6 Módulos de entrenamiento.</h2>
                        <span><i class="ion-ios-calendar-outline"></i>27 de Nov. al 04 de Mar. 2016</span>
                        <p>70 Mujeres entrenadas en Metodologías de innovación</p>
                       <!--  <span class="cd-date">12 horas</span> -->
                    </div> <!-- cd-timeline-content -->
                </div> <!-- cd-timeline-block -->
        
                <div class="cd-timeline-block">
                    <div class="cd-timeline-img cd-mod3">
                        <span><i class="ion-ios-compose"></i></span><!-- icon --> 
                    </div> <!-- cd-timeline-img -->
        
                    <div class="cd-timeline-content">
                        <h2>Formulación de Proyectos/Pitch Day.</h2>
                        <span><i class="ion-ios-calendar-outline"></i>Marzo a Junio 2016</span>
                        <p>30 Proyectos</p>
                    </div> <!-- cd-timeline-content -->
                </div> <!-- cd-timeline-block -->
        
                <div class="cd-timeline-block">
                    <div class="cd-timeline-img cd-mod4">
                        <span><i class="ion-cash"></i></span><!-- icon --> 
                    </div> <!-- cd-timeline-img -->
        
                    <div class="cd-timeline-content">
                        <h2>Levantamiento de Emprendimientos Dinámicos.</h2>
                        <span><i class="ion-ios-calendar-outline"></i>Marzo a Junio 2016</span>
                        <p>10 Nuevos emprendimientos Dinámicos con levantamiento de capital público/privado.</p>
                    </div> <!-- cd-timeline-content -->
                </div> <!-- cd-timeline-block -->
                
                 <div class="cd-timeline-block">
                    <div class="cd-timeline-img cd-mod5">
                        <span><i class="ion-earth"></i></span><!-- icon --> 
                    </div> <!-- cd-timeline-img -->
        
                    <div class="cd-timeline-content">
                        <h2>Gira Internacional de Innovación.</h2>
                        <span><i class="ion-ios-calendar-outline"></i>Abril a Mayo 2016</span>
                        <p>1 Gira a Medellín, Colombia</p>
                    </div> <!-- cd-timeline-content -->
                </div> <!-- cd-timeline-block -->

                 <div class="cd-timeline-block">
                    <div class="cd-timeline-img cd-mod5">
                        <span><i class="ion-earth"></i></span><!-- icon --> 
                    </div> <!-- cd-timeline-img -->
        
                    <div class="cd-timeline-content">
                        <h2>Precio.</h2>
                        <span><i class="ion-ios-calendar-outline"></i>Abril a Mayo 2016</span>
                        <p>1 Gira a Medellín, Colombia</p>
                    </div> <!-- cd-timeline-content -->
                </div> <!-- cd-timeline-block -->                

				</section> <!-- cd-timeline -->
			
		</section> <!-- #cd-placeholder-2 -->

		<section id="cd-placeholder-3" class="cd-section cd-container">
			<h2>Bases</h2>
			<p>
				Revisa aquí las bases del concurso. Una vez que postules, la organización asume que todos los postulantes conocen y aceptan las bases.			
			</p>
			<div class="download">
				<a href="BASES_MUJER_DINAMICA.pdf" class="cd-btn-download" target="_blank">Descargar</a>                    
			</div>
		</section> <!-- #cd-placeholder-3 -->
        
		<section id="cd-placeholder-4" class="cd-section cd-container">			
			<div class="side-left">
				<h2>Postulación</h2>
	            <p> Si crees que cumples con este perfil, ¡Debes Postular!</p>
					<br>
	                
	                <ul class="headline-info">
	                <li><span class="title">Mujeres mayores de 18 años</span></li>                
	                <li><span class="title">Con residencia en la región de Coquimbo</span></li>                
	                <li><span class="title">Sin iniciación de actividades</span></li> 
	                <li><span class="title">Con iniciación de actividades en  1ª y 2ª categoría (Multisectorial)</span></li> 
	                <li><span class="title">Educación básica y media completa, idealmente estudios superiores.</span></li> 
	                <li><span class="title">Conocimientos de computación (al menos básico)</span></li> 
	                <li><span class="title">Con motivación a emprender con innovación</span></li>                 
	            </ul>		
	        </div>
			<div class="side-right">	   			
				<img src="img/fondos/girl_desk_postulation.png" class="girl-desk-2" alt="Postulación Mujer Dinámica">
	        </div>



		</section> <!-- #cd-placeholder-4 -->        

		<section id="cd-placeholder-5" class="cd-section cd-container">
			<h2>Agenda</h2>
            <!-- CALENDARIO -->
           <br>	
			<section class="main">
			
					<div id="custom-inner" class="custom-inner">
						<div class="custom-header clearfix">
							<nav>
								<span id="custom-prev" class="custom-prev"></span>
								<span id="custom-next" class="custom-next"></span>
							</nav>
							<h2 id="custom-month" class="custom-month"></h2>
							<h3 id="custom-year" class="custom-year"></h3>
						</div>
						<div style="pointer-events:none;" id="calendar" class="fc-calendar-container"></div>
                        
					</div>
			
			</section>
            
            <div id="eventos">                

            </div>
            
               
      <!-- FIN CALENDARIO -->  
      
      
      




		</section> <!-- #cd-placeholder-5 -->

		<section id="cd-placeholder-6" class="cd-section cd-container">
			<h2>Últimas Noticias</h2>
            	<div id="grid-gallery" class="grid-gallery">
				<section class="grid-wrap">
				<?php 
				$array = array("");
				$sql_noticias = "SELECT * FROM NOTICIA";
				$result_noticias = gsql($sql_noticias, $array);

				?>
					<ul class="grid">
						<li class="grid-sizer"></li>
						<?php 
						while($datos_noticias = $result_noticias -> fetch(PDO::FETCH_ASSOC)){
							?>
								<li>
									<figure>
										<span class="date-news"><?php echo entrega_fecha3($datos_noticias['FECHA_INGRESO_NOT'])?></span>
										<a href="noticias/noticia.php?not=<?php echo $datos_noticias['ID_NOT']?>"><img src="Admin/noticias/portadas/<?php echo $datos_noticias['IMAGEN_NOT']?>" alt="img01"/></a>
										<figcaption>									
											<h3><?php echo $datos_noticias['NOMBRE_NOT']?>
											<span><?php echo $datos_noticias['CABECERA_NOT']?>
											</span></h3>
										</figcaption>
									</figure>
								</li>
							<?php 
						}
						?>					
         				             	                                                				
					</ul>


				</section><!-- // grid-wrap -->
			</div><!-- // grid-gallery -->
            

		</section> <!-- #cd-placeholder-6 -->
        
        <section id="cd-placeholder-7" class="cd-section cd-container">
			<h2>Staff</h2>
            <!-- Default info blocks -->
            <ul class="info-blocks"> 
           	<li class="bg-primary">
                <div class="top-info"><img src="img/fotos_equipo/Patricio_Herrera.png" alt="Alicia_Acuna"><span class="bottom-info bg-primary">Patricio Herrera G.</span><small>Gerente Codesser</small><small style="font-size:11px;">Director del Programa</small></div></li> 
              <li class="bg-primary">
                <div class="top-info"><img src="img/fotos_equipo/Karina_Salas.png" alt="Karina_Salas"><span class="bottom-info bg-primary">Karina Salas G.</span><small>Ingeniero Comercial, Mg.</small><small style="font-size:11px;">Coordinadora Startup</small></div></li>
              <li class="bg-primary">
                <div class="top-info"><img src="img/fotos_equipo/Valentina_Cortes.png" alt="Valentina_Cortes"><span class="bottom-info bg-primary">Valentina Cortés F.</span><small>Ingeniero Comercial</small><small style="font-size:11px;">Ejecutiva de Proyectos</small></div></li>
              <!--<li class="bg-primary">
                <div class="top-info"><img src="img/fotos_equipo/Carolina_Castillo.png" alt="Valentina_Cortes"><span class="bottom-info bg-primary">Carolina Castillo P.</span><small>Ingeniero Comercial</small><small style="font-size:11px;">Ejecutiva de Proyectos</small></div></li>-->
                <!--<li class="bg-primary">
                <div class="top-info"><img src="img/fotos_equipo/Veronica_Erler.png" alt="Veronica_Erler"><span class="bottom-info bg-primary">Verónica Erler S.</span><small>Ingeniero Agrónomo</small><small style="font-size:11px;">Asesora de Emprendimiento</small></div></li>-->
              <li class="bg-primary">
                <div class="top-info"><img src="img/fotos_equipo/Alicia_Acuna.png" alt="Alicia_Acuna"><span class="bottom-info bg-primary">Alicia Acuña G.</span><small>Periodista</small><small style="font-size:11px;">Periodista</small></div></li> 
                                         
            </ul>
            <!-- /default info blocks -->
              
          
		</section> <!-- #cd-placeholder-7 -->
        
        <footer>            
            <!-- Partners -->        
            <div class="footer-partners">        
                <!--<a href="http://www.corfo.cl/" target="_blank">
                    <img src="img/partners/corfo-bw2.png" alt="Academia" />
                </a> 
                <a href="http://www.gob.cl/" target="_blank">  
                    <img src="img/partners/gob-bw2.png" alt="Academia" />
                </a>-->
                <a href="http://www.corfo.cl/" target="_blank">
                    <img src="img/partners/corfo-gob.png" alt="Academia" style="height:200px;" />
                <a href="http://www.codesser.cl/regiones/coquimbo.php" target="_blank">
                    <img src="img/partners/codesser-bw2.png" alt="Academia" />		
                </a>                                  
            </div>        
            <!-- // Partners -->	  
            <div class="footer clearfix">
            
              <div class="icons-group">
              	<a href="https://plus.google.com/s/startup%20coquimbo" title="Google+" class="tip"><i class="ion-social-googleplus"></i></a>
                <a href="https://twitter.com/StartupCoquimbo" title="Twitter" class="tip"><i class="ion-social-twitter"></i></a> 
                <a href="https://www.facebook.com/startupcoquimbo" title="Facebook" class="tip"><i class="ion-social-facebook"></i></a> 
              </div>
              <ul id="copyright">              
                  <li><i class="icon-location3"></i>	
Balmaceda 391, Of. 214, piso 2. Edificio Italia, La Serena</li>
                  <li><i class="icon-phone"></i>051-2-211858  +56978774693 </li>
                  <li><a href="mailto:valentina.cortes@codesser.cl"><i class="icon-mail-send"></i>valentina.cortes@codesser.cl</a></li                                    
              ></ul>
              <p class="info">&copy; 2014 - Corporación de Desarrollo Social del Sector Rural - CODESSER</p>
              
              </div>


		    </div>
      
        </footer>    
        
        
        
            
	</main> <!-- .cd-main-content -->
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-throttle-debounce/1.1/jquery.ba-throttle-debounce.min.js"></script>
	<script src="js/table/jquery.stickyheader.js"></script>
    
        
	<script src="js/main.js"></script> <!-- Resource jQuery -->

        
    <script src="js/jquery-2.1.1.min.js"></script>
    
		<script src="js/imagesloaded.pkgd.min.js"></script>
		<script src="js/masonry.pkgd.min.js"></script>
		<script src="js/classie.js"></script>
		<script src="js/cbpGridGallery.js"></script>
		<script>
			new CBPGridGallery( document.getElementById( 'grid-gallery' ) );
		</script>
     
     <!-- CALENDARIO -->
		<script type="text/javascript" src="js/Calendario/js/jquery.calendario.js"></script>
		<script type="text/javascript" src="js/Calendario/js/data.js"></script>
		<script type="text/javascript">	
			$(function() {
				function cargar_eventos(mes, year){
					var dataString ='op=1&mes='+mes+'&year='+year;
					 $.ajax({
						url:'gr_eventos.php',
						type:'POST',
						data: dataString,
						success:function(datos){
							$('#eventos').html(datos);		
						}
					 });
				}
				
				
			
			
				var transEndEventNames = {
						'WebkitTransition' : 'webkitTransitionEnd',
						'MozTransition' : 'transitionend',
						'OTransition' : 'oTransitionEnd',
						'msTransition' : 'MSTransitionEnd',
						'transition' : 'transitionend'
					},
					transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
					$wrapper = $( '#custom-inner' ),
					$calendar = $( '#calendar' ),
					cal = $calendar.calendario( {
						onDayClick : function( $el, $contentEl, dateProperties ) {
							/*if( $contentEl.length > 0 ) {
								showEvents( $contentEl, dateProperties );
							}*/

						},
						caldata : codropsEvents,
						displayWeekAbbr : true
					} ),
					$month = $( '#custom-month' ).html( cal.getMonthName() ),
					$year = $( '#custom-year' ).html( cal.getYear() );
					cargar_eventos(cal.getMonth(), cal.getYear());

				$('.fc-row').children().hover(function(){
					//cal.gotoPreviousMonth( updateMonthYear );
					//$('#prueba').html(cal.getYear());
				});
				
					
				$( '#custom-next' ).on( 'click', function() {
					cal.gotoNextMonth( updateMonthYear );
					cargar_eventos(cal.getMonth(), cal.getYear());
				} );
				$( '#custom-prev' ).on( 'click', function() {
					cal.gotoPreviousMonth( updateMonthYear );
					cargar_eventos(cal.getMonth(), cal.getYear());

				} );

				function updateMonthYear() {				
					$month.html( cal.getMonthName() );
					$year.html( cal.getYear() );
				}

				// just an example..
				function showEvents( $contentEl, dateProperties ) {

					hideEvents();
					
					var $events = $( '<div id="custom-content-reveal" class="custom-content-reveal"><h4>Events for ' + dateProperties.monthname + ' ' + dateProperties.day + ', ' + dateProperties.year + '</h4></div>' ),
						$close = $( '<span class="custom-content-close"></span>' ).on( 'click', hideEvents );

					$events.append( $contentEl.html() , $close ).insertAfter( $wrapper );
					
					setTimeout( function() {
						$events.css( 'top', '0%' );
					}, 25 );

				}
				function hideEvents() {

					var $events = $( '#custom-content-reveal' );
					if( $events.length > 0 ) {
						
						$events.css( 'top', '100%' );
						Modernizr.csstransitions ? $events.on( transEndEventName, function() { $( this ).remove(); } ) : $events.remove();

					}

				}
			
			});
		</script>
 <!-- FIN CALENDARIO -->



    



</body>
</html>