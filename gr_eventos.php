<?php
session_start();
include("funciones/setup.php"); include("funciones/fecha.php");
ini_set('date.timezone', 'America/Buenos_Aires');

$gbd = conecta();

function gd($dato){ // get dato
  if(isset($_REQUEST[$dato]) and ($_REQUEST[$dato]!=NULL or $_REQUEST[$dato]!='')){
    return $_REQUEST[$dato]; 
  }
  return NULL;
}


function gsql($sql,$datos){ // get sql
  global $gbd;
  try{
    $sentencia = $gbd->prepare($sql);

    if(count($datos) == 0 or $datos == NULL){
      $sentencia->execute();
    }else{
      $sentencia->execute($datos);
    }
    return $sentencia;   
  }catch(Exception $e){
    echo "Error al generar la consulta";
  }
}


$op = (int)gd("op");
if(is_numeric($op))
switch($op) 
{
	case 1: obtenerEventos();
	break;
}

function obtenerEventos()
{
	 $mes = gd('mes');
	 $year = gd('year');
	 $arrdatos = array($mes, $year);
	 $sql_evento = "SELECT * FROM EVENTO WHERE month(FECHA_INGRESO_EVE) = ? and year(FECHA_INGRESO_EVE) = ? order by FECHA_INGRESO_EVE";	
	 $result_evento = gsql($sql_evento,$arrdatos);
	 $num_fre = $result_evento->rowCount();
  
  if($num_fre != 0){
		 $data = array();
		 ?>
         <div style="text-align:center; font-style:italic; ">Eventos para el mes de: <?php echo cambiar_mes2($mes); ?> </div><br>
		 <div class="component">
		 <table>
		 <thead>
			<tr style="font-size:13px;">
				<th>Fecha</th>
				<th>Evento</th>
				<th>Descripcion</th>
			</tr>
		  </thead>
		<?php 
		 while($datos_evento = $result_evento->fetch(PDO::FETCH_ASSOC)){
		?>
		<tbody>
			<tr align="center" class="reserva_titulo" style="font-size:12px;">
				<td>
					<input type="hidden" id="frm_id_evento" value="<?php echo $datos_evento['ID_EVE']; ?>" class="k-textbox"/>
					<input type="hidden" id="frm_fecha" value="<?php echo fecha_espanol($datos_evento['FECHA_INGRESO_EVE']); ?>" class="k-textbox"/>
					<?php echo entrega_fecha($datos_evento['FECHA_INGRESO_EVE'])." -"; echo " ".entrega_hora($datos_evento['HORA_INGRESO_EVE'])?>
			   </td>

			   <td><?php echo utf8_encode($datos_evento['NOMBRE_EVE'])?></td>
			  
			   <td><?php echo utf8_encode($datos_evento['DESCRIPCION_EVE'])?></td>
	
		   </tr>
		</tbody>
		<?php
		
		}
		?>
		</table>
		</div>
	<?php
    
    }else{
		?>
        <div style="text-align:center; font-style:italic; ">No existen eventos para el mes de: <?php echo cambiar_mes2($mes); ?> </div>
        <?php 
	}
}
    ?>