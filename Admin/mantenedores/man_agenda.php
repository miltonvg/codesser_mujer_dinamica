<?php
session_start();
include("../funciones/setup.php"); include("../funciones/fecha.php");
ini_set('date.timezone', 'America/Buenos_Aires');
$gbd = conecta();

if(isset($_SESSION['sesion_usuario_startup'])){

}else{
    header('Location: ../login.html');
}

function gd($dato){ // get dato
  if(isset($_REQUEST[$dato]) and $_REQUEST[$dato]!=NULL or $_REQUEST[$dato]!="" ){
    return $_REQUEST[$dato]; 
  }
  return NULL;
}


function gsql($sql,$datos){ // get sql
  global $gbd;
  try{
    $sentencia = $gbd->prepare($sql);

    if(count($datos) == 0 or $datos == NULL){
      $sentencia->execute();
    }else{
      $sentencia->execute($datos);
    }
    return $sentencia;   
  }catch(Exception $e){
    echo "Error al generar la consulta";
  }
}
?> 
<!DOCTYPE html>
 
<html lang="es">
 
<head>        
<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
		
<title>RESERVA</title>
<meta name="description" content="Codesser, Startup, Fase0" />
		<meta name="keywords" content="Codesser, Startup, Fase0" />
		<meta name="author" content="" />
        <script src="../js/modernizr.custom.63321.js"></script>
<link rel="shortcut icon" href="../images/favicon.png" />
<!-- Esto se especifica para que google encuentre donde esta alojada nuestra pagina web adaptada para móvil -->
<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.example.com/page-1" >

<!-- CALENDARIO -->
<link rel="stylesheet" type="text/css" href="../js/Calendario/css/calendar.css" />
<link rel="stylesheet" type="text/css" href="../js/Calendario/css/custom_2.css" />
		<script src="../js/Calendario/js/modernizr.custom.63321.js"></script>
<!-- FIN -->
<!--<style>
 @media only screen and (max-width: 640px) {...}
</style>-->
<link href="../css/estilos_forms.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../css/paginador_busqueda.css"/>
<link href="../css/estilos_base.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../css/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="../css/ionicons/css/ionicons.min.css">
</head>
<body>
<div id="cabecera">
<div id="credenciales">
      <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
          <td colspan="2" class="fuente_normal"><?php echo $_SESSION['sesion_usuario_startup'];?></td>
        </tr>
        <tr align="center">
              <td><a href="cerrar_sesion.php"><div class="button-close" ><i style="font-size:25px; color:#DD9696" class="ion-log-out"></i></div></a></td>
              <td><a href="../index.php"><div class="button-return" ><i style="font-size:25px; color:#DD9696" class="ion-arrow-left-a"></i></div></a></td>
            </tr>
            <tr>
            <td colspan="2"><span class="fuente_login">La Serena, <?php echo fecha_hoy();?>&nbsp;</span></td>
            </tr>
      </table>
    </div>
 </div>



<!-- CALENDARIO -->
<section class="main">			
		<div id="custom-inner" class="custom-inner">
			<div class="custom-header clearfix">
				<nav>
					<span id="custom-prev" class="custom-prev"></span>
					<span id="custom-next" class="custom-next"></span>
				</nav>
				<h2 id="custom-month" class="custom-month"></h2>
				<h3 id="custom-year" class="custom-year"></h3>
			</div>
			<div id="calendar" class="fc-calendar-container"></div>
		</div>
</section>
      <!-- FIN CALENDARIO -->      
<br />
<div id="grilla_reserva"></div>
<div class="grilla_reserva" id="grilla_evento"></div>
<div id="Pagination"></div>



<!-- CALENDARIO -->
		<!-- Javascript Libraries -->
        <script src="../js/jquery-2.1.1.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../vendors/nicescroll/jquery.nicescroll.min.js"></script>
        
        <script src="../vendors/auto-size/jquery.autosize.min.js"></script>
        <script src="../vendors/bootgrid/jquery.bootgrid.min.js"></script>
        <script src="../vendors/waves/waves.min.js"></script>
        <script src="../vendors/moment/moment.min.js"></script>
        <script src="../vendors/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
        <script src="../vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="../vendors/sweet-alert/sweet-alert.min.js"></script>


        <script src="../js/functions.js"></script>
        <script src="../js/demo.js"></script>
        <script src="../js/mantenedores/man_agenda.js"></script>

        <script src="../js/Calendario/js/jquery.calendario.js"></script>
		<script src="../js/Calendario/js/data.js"></script>
		<script type="text/javascript">	
			$(function() {
				var transEndEventNames = {
						'WebkitTransition' : 'webkitTransitionEnd',
						'MozTransition' : 'transitionend',
						'OTransition' : 'oTransitionEnd',
						'msTransition' : 'MSTransitionEnd',
						'transition' : 'transitionend'
					},
					transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
					$wrapper = $( '#custom-inner' ),
					$calendar = $( '#calendar' ),
					cal = $calendar.calendario( {
						onDayClick : function( $el, $contentEl, dateProperties ) {
							/*if( $contentEl.length > 0 ) {
								showEvents( $contentEl, dateProperties );
							}*/
						},
						caldata : codropsEvents,
						displayWeekAbbr : true
					} ),
					$month = $('#custom-month').html(cal.getMonthName()),
					$year = $('#custom-year').html(cal.getYear());

				$( '#custom-next' ).on( 'click', function() {
					cal.gotoNextMonth( updateMonthYear );
				} );
				$( '#custom-prev' ).on( 'click', function() {
					cal.gotoPreviousMonth( updateMonthYear );
				} );

				function updateMonthYear() {				
					$month.html( cal.getMonthName() );
					$year.html( cal.getYear() );
				}

				// just an example..
				function showEvents( $contentEl, dateProperties ) {

					hideEvents();
					
					var $events = $( '<div id="custom-content-reveal" class="custom-content-reveal"><h4>Events for ' + dateProperties.monthname + ' ' + dateProperties.day + ', ' + dateProperties.year + '</h4></div>' ),
						$close = $( '<span class="custom-content-close"></span>' ).on( 'click', hideEvents );

					$events.append( $contentEl.html() , $close ).insertAfter( $wrapper );
					
					setTimeout( function() {
						$events.css( 'top', '0%' );
					}, 25 );

				}
				function hideEvents() {

					var $events = $( '#custom-content-reveal' );
					if( $events.length > 0 ) {
						
						$events.css( 'top', '100%' );
						Modernizr.csstransitions ? $events.on( transEndEventName, function() { $( this ).remove(); } ) : $events.remove();

					}

				}
			
			});
		</script>
 <!-- FIN CALENDARIO -->
</body>
</html>
