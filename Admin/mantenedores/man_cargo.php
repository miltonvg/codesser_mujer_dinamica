<?php
session_start();
include("../funciones/setup.php"); include("../funciones/fecha.php");
ini_set('date.timezone', 'America/Buenos_Aires');
$gbd = conecta();

if(isset($_SESSION['sesion_usuario_startup'])){

}else{
    header('Location: ../login.html');
}

function gd($dato){ // get dato
  if(isset($_REQUEST[$dato]) and $_REQUEST[$dato]!=NULL or $_REQUEST[$dato]!="" ){
    return $_REQUEST[$dato]; 
  }
  return NULL;
}


function gsql($sql,$datos){ // get sql
  global $gbd;
  try{
    $sentencia = $gbd->prepare($sql);

    if(count($datos) == 0 or $datos == NULL){
      $sentencia->execute();
    }else{
      $sentencia->execute($datos);
    }
    return $sentencia;   
  }catch(Exception $e){
    echo "Error al generar la consulta";
  }
}
?> 
<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Startup Mujer Dinámica</title>

        <!-- Vendor CSS -->
        <link href="../vendors/bootgrid/jquery.bootgrid.min.css" rel="stylesheet">        
        <link href="../vendors/animate-css/animate.min.css" rel="stylesheet">
        <link href="../vendors/sweet-alert/sweet-alert.min.css" rel="stylesheet">
        <link href="../vendors/material-icons/material-design-iconic-font.min.css" rel="stylesheet">
        <link href="../vendors/socicon/socicon.min.css" rel="stylesheet">
            
        <!-- CSS -->
        <link href="../css/app.min.1.css" rel="stylesheet">
        <link href="../css/app.min.2.css" rel="stylesheet">

        
    </head>
    <body class="toggled sw-toggled">
        <header id="header">
            <ul class="header-inner">
                
            
                <li class="logo hidden-xs">
                    <a href="../index.php"><img style="width:100px;" src="../../img/logo_full.png"></a>
                </li>
                
                <li class="pull-right">
                <ul class="top-menu">

                    <li class="dropdown">
                        <a data-toggle="dropdown" class="tm-settings" href="#"></a>
                        <ul class="dropdown-menu dm-icon pull-right">
                            <li>
                                <a data-action="fullscreen" href="#"><i class="md md-fullscreen"></i> Pantalla Completa</a>
                            </li>                            

                        </ul>
                    </li>
                    </ul>
                </li>
            </ul>
            

        </header>
        
        
        <section id="main">            
         
        
                       <section id="content">
                <div class="container">
                    <div class="block-header">
                        <h2>Mantenedor de Cargos</h2>
                    </div>
                
                    <div class="card">

                        <form role="form">
                            <input id="id_car" type="hidden" class="form-control">
                            <div class="card-header">
                                <h2>Registro de Cargo</h2>
                            </div>
                            
                            <div class="card-body card-padding">
                              
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="" class="control-label">Nombre</label>
                                            <div class="fg-line">
                                                <input onkeyup="javascript:this.value=this.value.toUpperCase();" id="nombre_car" type="text" class="form-control">                                                
                                            </div>                                            
                                        </div>    
                                                                                                             
                                    </div>
                                </div>         
                                                                                     
                                <div class="form-group m-t-40">
                                    <div class="row">
                                        <div class="col-sm-offset-1 col-sm-5">
                                            <button type="submit" class="btn btn-default btn-block btn-sm waves-effect waves-button waves-float">Cancelar</button>
                                        </div>
                                        <div class="col-sm-5">
                                           <a id="btn_ingresar" class="btn btn-primary btn-block  btn-sm waves-effect waves-button waves-float">Registrar</a>                                        
                                        </div> 

                                        <div class="col-sm-5">
                                           <a id="btn_modificar" class="btn btn-warning btn-block  btn-sm waves-effect waves-button waves-float">Guardar Cambios</a>                                        
                                        </div>  

                                        <!--<div class="col-sm-offset-1 col-sm-1">
                                           <button id="btn_eliminar" class="btn btn-primary btn-sm waves-effect waves-button waves-float">Eliminar</button>                                        
                                        </div>-->      
                                    </div>                           
                                </div> 
                            </div>
                        </form>                    

                    </div>

                    <div class="card">

                            <div class="card-header">
                                <h2></i>Listado de cargo</h2>
                            </div>
                            <div class="card-body card-padding">
                                <div class="table-responsive">
                                    <table id="data-table-command" class="table table-striped table-vmiddle">
                                        <thead>
                                            <tr>
                                                <th data-column-id="id" data-formatter="hidden"></th>
                                                <th data-column-id="2">Nombre</th>
                                                <th data-column-id="commands" data-formatter="commands" data-sortable="false">Acción</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                 $arrdatos_car = array('');
                                                    $sql_car = "SELECT * FROM CARGO";
                                                    $result_car = gsql($sql_car,$arrdatos_car);
                                                    while($datos_car = $result_car->fetch(PDO::FETCH_ASSOC)){

                                            ?>
                                            <tr>
                                                <td><?php echo $datos_car['ID_CAR']; ?></td>
                                                <td><?php echo $datos_car['NOMBRE_CAR']; ?></td>
                                            </tr>
                                            <?php
                                                }
                                            ?>                                                             
                                        </tbody>
                                    </table>                                  
                                </div>
                            </div>

                          

                    </div>                    
                    



                </div>
            </section>
        </section>
    
        
       
        <!-- Javascript Libraries -->
        <script src="../js/jquery-2.1.1.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../vendors/nicescroll/jquery.nicescroll.min.js"></script>
        
        
        <script src="../vendors/auto-size/jquery.autosize.min.js"></script>
        <script src="../vendors/bootgrid/jquery.bootgrid.min.js"></script>
        <script src="../vendors/waves/waves.min.js"></script>
        <script src="../vendors/moment/moment.min.js"></script>
        <script src="../vendors/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
        <script src="../vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="../vendors/sweet-alert/sweet-alert.min.js"></script>


        <script src="../js/functions.js"></script>
        <script src="../js/demo.js"></script>
        <script src="../js/mantenedores/man_cargo.js"></script>
    
    



        <!-- Javascript Libraries -->

        

        <!-- Data Table -->
        <script type="text/javascript">
            $(document).ready(function(){
                
                
                $("#btn_eliminar").hide();
                $("#btn_modificar").hide();
                

                $("#btn_ingresar").click(function(){
                    validar(1);
                });
                $("#btn_modificar").click(function(){
                    validar(2);
                });

                $("#btn_eliminar").click(function(){
                    eliminar();
                });

                //Basic Example
                $("#data-table-basic").bootgrid({
                    css: {
                        icon: 'md icon',
                        iconColumns: 'md-view-module',
                        iconDown: 'md-expand-more',
                        iconRefresh: 'md-refresh',
                        iconUp: 'md-expand-less'
                    },
                });
                
                //Selection
                $("#data-table-selection").bootgrid({
                    css: {
                        icon: 'md icon',
                        iconColumns: 'md-view-module',
                        iconDown: 'md-expand-more',
                        iconRefresh: 'md-refresh',
                        iconUp: 'md-expand-less'
                    },
                    selection: true,
                    multiSelect: true,
                    rowSelect: true,
                    keepSelection: true
                });
                
                //Command Buttons
                $("#data-table-command").bootgrid({
                    css: {
                        icon: 'md icon',
                        iconColumns: 'md-view-module',
                        iconDown: 'md-expand-more',
                        iconRefresh: 'md-refresh',
                        iconUp: 'md-expand-less'
                    },
                    formatters: {
                        "commands": function(column, row) {
                            return "<button type=\"button\" class=\"btn btn-table command-edit\" data-row-id=\"" + row.id + "\" onClick=\"cargar_datos("+row.id+")\"><span class=\"md md-create\"></span></button> " + 
                                "<button type=\"button\" class=\"btn btn-table command-delete\" data-row-id=\"" + row.id + "\" onClick=\"eliminar("+row.id+")\"><span class=\"md md-delete\"></span></button>";
                        },
                        "hidden": function(column, row){
                            return "";
                        }
                    }
                });
            });
        </script>







    </body>


</html>