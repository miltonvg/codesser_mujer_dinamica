<?php
session_start();
include("../funciones/setup.php"); include("../funciones/fecha.php");
ini_set('date.timezone', 'America/Buenos_Aires');
$gbd = conecta();

if(isset($_SESSION['sesion_usuario_startup'])){

}else{
    header('Location: ../login.html');
}

function gd($dato){ // get dato
  if(isset($_REQUEST[$dato]) and $_REQUEST[$dato]!=NULL or $_REQUEST[$dato]!="" ){
    return $_REQUEST[$dato]; 
  }
  return NULL;
}


function gsql($sql,$datos){ // get sql
  global $gbd;
  try{
    $sentencia = $gbd->prepare($sql);

    if(count($datos) == 0 or $datos == NULL){
      $sentencia->execute();
    }else{
      $sentencia->execute($datos);
    }
    return $sentencia;   
  }catch(Exception $e){
    echo "Error al generar la consulta";
  }
}

$opcion=gd('op');

switch($opcion) 
{
	case 1: ingresar();
	break;
	case 2: modificar();
	break;
	case 3: eliminar();
	break;
	case 99: cargar_datos();
	break;

}
function ingresar(){

	$usuario_usu = $_POST['usuario_usu'];
	$clave_usu = $_POST['clave_usu'];
	$nombre_usu = $_POST['nombre_usu'];
	$id_car = $_POST['id_car'];

	$array = array($usuario_usu, $clave_usu, $nombre_usu, $id_car);

	$sql = "INSERT INTO USUARIO SET USUARIO_USU=?, CLAVE_USU = ?, NOMBRE_USU = ?, ID_CAR = ?";
 	gsql($sql,$array);
}

function modificar()
{

	$id_usu = $_POST['id_usu'];
	$usuario_usu = $_POST['usuario_usu'];
	$clave_usu = $_POST['clave_usu'];
	$nombre_usu = $_POST['nombre_usu'];
	$id_car = $_POST['id_car'];

	$array = array($usuario_usu, $clave_usu, $nombre_usu, $id_car, $id_usu);

	$sql="UPDATE  USUARIO SET USUARIO_USU = ?,CLAVE_USU = ?, NOMBRE_USU = ?, ID_CAR = ? WHERE ID_USU = ?";	
	gsql($sql,$array);
}

function eliminar()
{
	$id_usu = $_POST['id_usu'];
	$array = array($id_usu);
	$sql="DELETE FROM USUARIO WHERE  ID_USU = ?";
	gsql($sql,$array);
}

function cargar_datos(){
 
 $id_usu = (int)gd('id_usu');
 $sql_usu ="SELECT * FROM USUARIO where ID_USU = ? ";
 $result_usu = gsql($sql_usu, array($id_usu));
 $data = array();
 while($datos_usu=$result_usu->fetch(PDO::FETCH_ASSOC)){
    $data[] =array('id_usu' => $datos_usu['ID_USU'], 'usuario_usu' => $datos_usu['USUARIO_USU'], 'clave_usu' => $datos_usu['CLAVE_USU'], 'nombre_usu' => $datos_usu['NOMBRE_USU'], 'id_car' => $datos_usu['ID_CAR']);  
       
 }
 echo json_encode($data);
}

?>
