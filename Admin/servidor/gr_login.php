<?php
session_start();
include("../funciones/setup.php"); include("../funciones/fecha.php");
ini_set('date.timezone', 'America/Buenos_Aires');
$gbd = conecta();

if(isset($_SESSION['sesion_usuario_startup'])){

}else{
    header('Location: ../login.html');
}

function gd($dato){ // get dato
  if(isset($_REQUEST[$dato]) and $_REQUEST[$dato]!=NULL or $_REQUEST[$dato]!="" ){
    return $_REQUEST[$dato]; 
  }
  return NULL;
}


function gsql($sql,$datos){ // get sql
  global $gbd;
  try{
    $sentencia = $gbd->prepare($sql);

    if(count($datos) == 0 or $datos == NULL){
      $sentencia->execute();
    }else{
      $sentencia->execute($datos);
    }
    return $sentencia;   
  }catch(Exception $e){
    echo "Error al generar la consulta";
  }
}

$usuario_usu = $_POST['usuario_usu'];
$contrasena_usu = $_POST['contrasena_usu'];

$array = array($usuario_usu, $contrasena_usu);
    
 $sql_login = "SELECT * FROM USUARIO where USUARIO_USU=? and CLAVE_USU =?";
 $result_login = gsql($sql_login,$array);
 $datos = $result_login->fetch(PDO::FETCH_ASSOC);
 $total = $result_login->rowCount();

if($total==0)
{
	echo 0;
	exit;
}
else
{
	echo 1;

	$_SESSION['sesion_usuario_startup']=$datos['NOMBRE_USU'];
	$_SESSION['id_usu_startup'] = $datos['ID_USU'];
	$_SESSION['sesion_tipo_startup']=$datos['ID_CAR'];

	exit;
}

?>