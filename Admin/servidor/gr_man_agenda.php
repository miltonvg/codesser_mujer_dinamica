<?php
session_start();
include("../funciones/setup.php"); include("../funciones/fecha.php");
ini_set('date.timezone', 'America/Buenos_Aires');
$gbd = conecta();

if(isset($_SESSION['sesion_usuario_startup'])){

}else{
    header('Location: ../login.html');
}

function gd($dato){ // get dato
  if(isset($_REQUEST[$dato]) and $_REQUEST[$dato]!=NULL or $_REQUEST[$dato]!="" ){
    return $_REQUEST[$dato]; 
  }
  return NULL;
}


function gsql($sql,$datos){ // get sql
  global $gbd;
  try{
    $sentencia = $gbd->prepare($sql);

    if(count($datos) == 0 or $datos == NULL){
      $sentencia->execute();
    }else{
      $sentencia->execute($datos);
    }
    return $sentencia;   
  }catch(Exception $e){
    echo "Error al generar la consulta";
  }
}

$opcion=gd('op');


switch($opcion) 
{
	case 1: ingresar();
	break;
	case 2: modificar();
	break;
	case 3: eliminar();
	break;
	case 4: muestra_grilla();
	break;
	case 5: muestra_evento();
	break;
}

function ingresar()
{
	$fecha_ingreso_eve = $_POST['fecha_ingreso_eve'];
	$hora_ingreso_eve = $_POST['hora_ingreso_eve'];
	$nombre_eve = $_POST['nombre_eve'];
	$descripcion_eve = $_POST['descripcion_eve'];
	
		  
	$array = array($fecha_ingreso_eve, $hora_ingreso_eve, $nombre_eve, $descripcion_eve);
  $sql="INSERT INTO EVENTO (FECHA_INGRESO_EVE, HORA_INGRESO_EVE, NOMBRE_EVE, DESCRIPCION_EVE)values(?,?,?,?)";
	gsql($sql, $array);
}
function modificar()
{
	$id_eve = $_POST['id_eve'];
	$fecha_ingreso_eve = $_POST['fecha_ingreso_eve'];
	$hora_ingreso_eve = $_POST['hora_ingreso_eve'];
	$nombre_eve = $_POST['nombre_eve'];
	$descripcion_eve = $_POST['descripcion_eve'];
		  
	$array_update = array($fecha_ingreso_eve, $hora_ingreso_eve, $nombre_eve, $descripcion_eve, $id_eve);
  $sql="UPDATE EVENTO set FECHA_INGRESO_EVE=?, HORA_INGRESO_EVE=?, NOMBRE_EVE=?, DESCRIPCION_EVE=? where ID_EVE = ?";
	gsql($sql, $array_update);
}

function eliminar()
{
	$id_eve = $_POST['id_eve'];
  $array_eve = array($id_eve);
	$sql1="DELETE from EVENTO where ID_EVE=?";
	gsql($sql1, $array_eve);
}

function muestra_evento(){
	$fecha_ingreso_eve = $_POST['fecha'];
	$id_eve = $_POST['id_eve'];
  $array_eve = array($id_eve);
	$sql_evento = "SELECT * FROM EVENTO WHERE ID_EVE = ? limit 1";
	$result_evento = gsql($sql_evento, $array_eve);
	$datos_evento = $result_evento -> fetch(PDO::FETCH_ASSOC);
	
	?>
        <table style="text-align:left;" class="fuente_formularios" width="100%" border="0" cellspacing="5" cellpadding="0">
         <tr align="center" class="reserva_nombre_cancha">
            <td colspan="2">Evento</td>
          </tr>
          <tr>
            <td>Hora</td>
            <td>
            	<input type="hidden" id="frm_id_eve" value="<?php if($id_eve != 0){ echo $id_eve; } ?>" class="k-textbox"/>
            	<input type="hidden" id="frm_fecha_ingreso_eve" value="<?php echo $fecha_ingreso_eve; ?>" class="k-textbox"/>
              <input name="frm_hora" type="time" id="frm_hora" value="<?php if($id_eve != 0){ echo $datos_evento['HORA_INGRESO_EVE']; }?>" class="k-textbox"/>
           </td>
            </tr>
            <tr>
            <td>Evento</td>
            <td><input name="frm_nombre" type="text" id="frm_nombre" value="<?php if($id_eve != 0){ echo $datos_evento['NOMBRE_EVE']; }?>" class="k-textbox"/></td>
          
            </tr>
          <tr>
            <td>Descripción</td>
            <td>
              <textarea cols="78" rows="1" name="frm_descripcion"  id="frm_descripcion" class="k-textbox"><?php if($id_eve != 0){ echo $datos_evento['DESCRIPCION_EVE']; }?> </textarea>
            </td>
          </tr>
    
            <td colspan="4"><br/><hr color="#FFFFFF" size="1" /></td>
          </tr>
          <tr>
            <td colspan="4" align="center">
              <?php
                if($id_eve == 0)
                {
                    ?>
              <input onClick="validar(1)" type="button" name="btn_ingresar" id="btn_ingresar" value="Ingresar" class="g-button-submit"/>
    &nbsp;
    <?php
                }
                else
                {?>
                <input onClick="validar(2)" type="button" name="btn_modificar" id="btn_modificar" value="Modificar" class="g-button-submit"/>
    &nbsp;
    <input onClick="eliminar_evento()" type="button" name="btn_eliminar" id="btn_eliminar" value="Eliminar" class="g-button-submit"/>
    &nbsp;
    <?php
                }
                ?>
    <input onClick="cancelar_evento()" type="button" name="btn_cancelar" id="btn_cancelar" value="Cancelar" class="g-button-submit"/></td>
          </tr>
        </table>
        <?php 
}

function muestra_grilla()
{
	$fecha_dia = $_POST['fecha_dia'];

//CONSULTAR SI HAY DATOS EN LA BD, SI NO LOS HOY, LLENARLA
	
$array = array($fecha_dia);
$sql_evento = "SELECT * FROM EVENTO WHERE FECHA_INGRESO_EVE =?";
$result_evento = gsql($sql_evento, $array);
?>
<div id="reserva_hora">
<table width="500px" class="fuente_formularios" border="0">
    <tr align="center" class="reserva_nombre_cancha">
    <td colspan="8">Eventos para el día: <?php echo $fecha_dia;?></td>
    </tr>
    <tr>
    	 <td>Fecha</td>
        <td>Hora</td>
        <td>Evento</td>
        <td>Descripcion</td>
        <td>Accion</td>
    </tr>
<?php 
while($datos_evento = $result_evento -> fetch(PDO::FETCH_ASSOC)){
?>
    <tr align="center" class="reserva_titulo">
        <td>
        	<input type="hidden" id="frm_id_eve" value="<?php echo $datos_evento['ID_EVE']; ?>" class="k-textbox"/>
        	<input type="hidden" id="fecha_ingreso_eve" value="<?php echo $datos_evento['FECHA_INGRESO_EVE']; ?>" class="k-textbox"/>
			<?php echo $datos_evento['FECHA_INGRESO_EVE']?>
       </td>
        
       <td><?php echo $datos_evento['HORA_INGRESO_EVE']?></td>
        
       <td><?php echo $datos_evento['NOMBRE_EVE']?></td>
      
       <td><?php echo $datos_evento['DESCRIPCION_EVE']?></td>
       
        <td style="background-color: ghostwhite">
          <a onClick="datos_evento(<?php echo $datos_evento['ID_EVE'];?>, '<?php echo $fecha_dia;?>');" href="#"><i style="font-size:25px; color:#3C6" class="ion-checkmark-round"></i></a>&nbsp; 
          <a href="#" onclick="eliminar_evento(<?php echo $datos_evento['ID_EVE']; ?>)"><i style="font-size:25px; color:#666" class="ion-trash-a"></i></a>
        </td>
        
   </tr>
<?php

}
?>
</table>
<br>
<div style="text-align:left;">
	<input onClick="datos_evento(0, '<?php echo $fecha_dia;?>');" type="button" value="Nuevo Evento" class="g-button-submit">
</div><br>
<?php 
}
?>

</div>

