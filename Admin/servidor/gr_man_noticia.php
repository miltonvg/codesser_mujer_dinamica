<?php
session_start();
include("../funciones/setup.php"); include("../funciones/fecha.php");
ini_set('date.timezone', 'America/Buenos_Aires');
$gbd = conecta();

if(isset($_SESSION['sesion_usuario_startup'])){

}else{
    header('Location: ../login.html');
}

function gd($dato){ // get dato
  if(isset($_REQUEST[$dato]) and $_REQUEST[$dato]!=NULL or $_REQUEST[$dato]!="" ){
    return $_REQUEST[$dato]; 
  }
  return NULL;
}


function gsql($sql,$datos){ // get sql
  global $gbd;
  try{
    $sentencia = $gbd->prepare($sql);

    if(count($datos) == 0 or $datos == NULL){
      $sentencia->execute();
    }else{
      $sentencia->execute($datos);
    }
    return $sentencia;   
  }catch(Exception $e){
    echo "Error al generar la consulta";
  }
}

$opcion=gd('op');

switch($opcion) 
{
	case 1: ingresar();
	break;
	case 2: modificar();
	break;
	case 3: eliminar();
	break;
	case 99: cargar_datos();
	break;

}
function ingresar(){
	

	$usuario_not = $_POST['usuario_not'];
	$nombre_not = $_POST['nombre_not'];
	$fecha_ingreso_not = fecha_database($_POST['fecha_ingreso_not']);
	$descripcion_not = $_POST['descripcion_not'];
	$cabecera_not = $_POST['cabecera_not'];

	$array = array($usuario_not, $nombre_not, $fecha_ingreso_not, $cabecera_not);

	$sql = "INSERT INTO NOTICIA SET USUARIO_NOT=?, NOMBRE_NOT = ?, FECHA_INGRESO_NOT = ?, CABECERA_NOT=?";
 	gsql($sql,$array);

 	//ULTIMO ID INGRESADO
 	$sql = "SELECT last_insert_id() as LastId";
 	$result = gsql($sql, $array);
 	$datos = $result->fetch(PDO::FETCH_ASSOC);
 	$id_not = ((int)$datos['LastId']);

 	//PRUBEA DE CREACION DE ARCHIVO

	$nombre_archivo ="../noticias/".$id_not.".txt";
	$myfile = fopen($nombre_archivo, "w");
	fwrite($myfile, $descripcion_not);
	fclose($myfile);

	$data = array();
	$array_datos_macro = array('id_not'=> $id_not);
	array_push($data, $array_datos_macro);
	echo json_encode($data);

}

function modificar()
{

	$id_not = (int)$_POST['id_not'];
	$usuario_not = $_POST['usuario_not'];
	$nombre_not = $_POST['nombre_not'];
	$fecha_ingreso_not = fecha_database($_POST['fecha_ingreso_not']);
	$descripcion_not = $_POST['descripcion_not'];
	$cabecera_not = $_POST['cabecera_not'];

	$nombre_archivo ="../noticias/".$id_not.".txt";
	$myfile = fopen($nombre_archivo, "w");
	fwrite($myfile, $descripcion_not);
	fclose($myfile);

	$array = array($usuario_not, $nombre_not, $fecha_ingreso_not, $cabecera_not, $id_not);

	$sql="UPDATE  NOTICIA SET USUARIO_NOT = ?,NOMBRE_NOT = ?, FECHA_INGRESO_NOT = ?, CABECERA_NOT=? WHERE ID_NOT = ?";	
	gsql($sql,$array);
}

function eliminar()
{
	$id_not = (int)$_POST['id_not'];
	$array = array($id_not);
	$sql="DELETE FROM NOTICIA WHERE  ID_NOT = ?";
	gsql($sql,$array);
}

function cargar_datos(){
 
 $id_not = (int)gd('id_not');
 $sql_not ="SELECT * FROM NOTICIA WHERE ID_NOT = ? ";
 $result_not = gsql($sql_not, array($id_not));
 $data = array();
 while($datos_not=$result_not->fetch(PDO::FETCH_ASSOC)){
 	

 	// get contents of a file into a string
		$nombre_archivo ="../noticias/".$id_not.".txt";
		$handle = fopen($nombre_archivo, "r");
		$descripcion_not = fread($handle, filesize($nombre_archivo));
		$descripcion_not = stripslashes($descripcion_not);
		fclose($handle);


    $data[] =array('id_not' => $datos_not['ID_NOT'], 'usuario_not' => $datos_not['USUARIO_NOT'], 'nombre_not' => $datos_not['NOMBRE_NOT'], 'fecha_ingreso_not' => fecha_espanol($datos_not['FECHA_INGRESO_NOT']), 'descripcion_not' => $descripcion_not, 'cabecera_not' => $datos_not['CABECERA_NOT']);  
       
 }
 echo json_encode($data);
}

?>
