<?php
session_start();
include("../funciones/setup.php"); include("../funciones/fecha.php");
ini_set('date.timezone', 'America/Buenos_Aires');
$gbd = conecta();

if(isset($_SESSION['sesion_usuario_startup'])){

}else{
    header('Location: ../login.html');
}

function gd($dato){ // get dato
  if(isset($_REQUEST[$dato]) and $_REQUEST[$dato]!=NULL or $_REQUEST[$dato]!="" ){
    return $_REQUEST[$dato]; 
  }
  return NULL;
}


function gsql($sql,$datos){ // get sql
  global $gbd;
  try{
    $sentencia = $gbd->prepare($sql);

    if(count($datos) == 0 or $datos == NULL){
      $sentencia->execute();
    }else{
      $sentencia->execute($datos);
    }
    return $sentencia;   
  }catch(Exception $e){
    echo "Error al generar la consulta";
  }
}

$opcion=gd('op');

switch($opcion) 
{
	case 1: ingresar();
	break;
	case 2: modificar();
	break;
	case 3: eliminar();
	break;
	case 99: cargar_datos();
	break;

}
function ingresar(){

	$nombre_car = $_POST['nombre_car'];

	$array = array($nombre_car);
	$sql = "INSERT INTO CARGO SET NOMBRE_CAR=?";
 	gsql($sql,$array);
}

function modificar()
{
	$id_car = $_POST['id_car'];
	$nombre_car = $_POST['nombre_car'];

	$array = array($nombre_car, $id_car);
	$sql="UPDATE  CARGO SET NOMBRE_CAR = ? WHERE ID_CAR = ?";	
	gsql($sql,$array);
}

function eliminar()
{
	$id_car = $_POST['id_car'];
	$array = array($id_car);
	$sql="DELETE FROM CARGO WHERE  ID_CAR = ?";
	gsql($sql,$array);
}

function cargar_datos(){
 
 $id_car = (int)gd('id_car');
 $sql_usu ="SELECT * FROM CARGO WHERE ID_CAR = ? ";
 $result_usu = gsql($sql_usu, array($id_car));
 $data = array();
 while($datos_usu=$result_usu->fetch(PDO::FETCH_ASSOC)){
    $data[] =array('id_car' => $datos_usu['ID_CAR'], 'nombre_car' => $datos_usu['NOMBRE_CAR']);  
       
 }
 echo json_encode($data);
}

?>
