// JavaScript Document
function validar(valor)
{
	if($('#nombre_car').val()=="")
	{
		alert("Debe ingresar el Nombre");
		$('#nombre_car').focus();
		return false;
	}


	if(valor==1) ingresar();
	if(valor==2) modificar();
	
}

function ingresar()
{
	var nombre_car = $('#nombre_car').val();

	var dataString = 'op=1&nombre_car='+ nombre_car;
	   $.ajax({
			url:'../servidor/gr_man_cargo.php',
			type:'POST',
			data:dataString,
			success:function(datos){
				alert("Cargo Ingresado  con Exito");
				location.href="man_cargo.php";
			}
		});
}
function modificar()
{
	var nombre_car = $('#nombre_car').val();
	var id_car = $('#id_car').val();

	var dataString = 'op=2&nombre_car='+ nombre_car+ '&id_car=' + id_car;
	console.log(dataString);   
	$.ajax({
		url:'../servidor/gr_man_cargo.php',
		type:'POST',
		data: dataString,
		success:function(datos){

			//alert("Cargo Modificado con Exito");
			//location.href="man_cargo.php";
		}
	});
}


function eliminar(id_car)
{
	
	if(id_car==null){ 
		var id_car = $("#id_car").val(); 
	}
	var dataString = 'op=3&id_car='+ id_car;
	  swal({   
			title: "Eliminar Registro",   
			text: "Este registro no volverá a estar disponible.",   
			type: "warning",   
			showCancelButton: true,   
			confirmButtonColor: "#DD6B55",   
			confirmButtonText: "Si, Eliminar!",   
			closeOnConfirm: true,
			cancelButtonText: 'Cancelar'
		}, function(){  
			$.ajax({
					url:'../servidor/gr_man_cargo.php',
					type:'POST',
					data:dataString,
					success:function(datos){
						swal("Deleted!", "Your imaginary file has been deleted.", "success");
						//alert("Banco Eliminado");
						location.href="man_cargo.php";
					}
				}); 
			 
		});
}

 function cargar_datos(id_car){
    var dataString = 'op=99&id_car='+ id_car;
	   $.ajax({
		url:'../servidor/gr_man_cargo.php',
		dataType:'json',
		type:'POST',
		data:dataString,
		success:function(datos){
			$('#id_car').val(datos[0].id_car);
			$('#nombre_car').val(datos[0].nombre_car);

			$('#btn_ingresar').parent().css('display', 'none');	
			$('#btn_modificar').show();
			$('#btn_eliminar').show();
			$(document.body).animate({
			    'scrollTop':   $('#content').offset().top
			}, 500);

		}
	});
}

 