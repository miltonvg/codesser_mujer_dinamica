// JavaScript Document
function validar(valor)
{
	if($('#nombre_not').val()=="")
	{
		alert("Debe ingresar el nombre de la noticia");
		$('#nombre_not').focus();
		return false;
	}

	if($('#descripcion_not').val()=="")
	{
		alert("Debe ingresar una descripción de la noticia");
		$('#descripcion_not').focus();
		return false;
	}

	if(valor==1) ingresar();
	if(valor==2) modificar();
	
}

 function sendFile(file, url, editor) {
 	swal({   title: "Espere un momento!",   text: "Subiendo imagen.",  showConfirmButton: false });
    var data = new FormData();
    data.append("file", file);
    swal({   title: "Cargando!",   text: "Espere mientras se sube la imagen", showConfirmButton: false });
    $.ajax({
        data: data,
        type: "POST",
        url: '../servidor/gr_upload_image.php',
        cache: false,
        contentType: false,
        processData: false,
        success: function(objFile) {
        	swal.close();
        	console.log(objFile);
            //$('#summernote').summernote('insertNode', url);
            editor.summernote('insertImage', objFile);
        },
	    error: function(jqXHR, textStatus, errorThrown)
	    {
	    }
    });
}


function ingresar()
{
	var nombre_not = $('#nombre_not').val();
	var fecha_ingreso_not = $('#fecha_ingreso_not').val();
	var usuario_not = $('#usuario_not').val();
	// var descripcion_not = $('#descripcion_not').val();
	var cabecera_not = $('#cabecera_not').val();
	var descripcion_not = $('#summernote').code();



	swal({   title: "Espere un momento!",   text: "Subiendo imagenen de portada.",  showConfirmButton: false });
	var dataString = 'op=1&nombre_not='+ nombre_not + '&usuario_not=' + usuario_not + '&fecha_ingreso_not='+ fecha_ingreso_not + '&descripcion_not=' + descripcion_not + '&cabecera_not='+ cabecera_not;
	   $.ajax({
			url:'../servidor/gr_man_noticia.php',
			datatype:'json',
			type:'POST',
			data:dataString,
			success:function(datos){
				datos = JSON.parse(datos);
				var id_not = datos[0].id_not;
				var file = $('#imagen_not')[0].files[0];
				var data = new FormData();
    			data.append("file", file);
    			data.append("id_not", id_not);				
			    $.ajax({
			        data: data,
			        type: "POST",
			        url: '../servidor/gr_upload_portada.php',
			        cache: false,
			        contentType: false,
			        processData: false,
			        success: function(objFile) {
			        	swal.close();
			         	alert("Noticia Ingresada con Exito");
						 location.href="man_noticia.php";

			            //$('#summernote').summernote('insertNode', url);
			            // editor.summernote('insertImage', objFile);
			        },
				    error: function(jqXHR, textStatus, errorThrown)
				    {
				    }
			    });
			}
		});
}
function modificar()
{
	var id_not = $('#id_not').val();
	var nombre_not = $('#nombre_not').val();
	var fecha_ingreso_not = $('#fecha_ingreso_not').val();
	var usuario_not = $('#usuario_not').val();
	// var descripcion_not = $('#descripcion_not').val();
	var descripcion_not = $('#summernote').code();
	var cabecera_not = $('#cabecera_not').val();

	var dataString = 'op=2&nombre_not='+ nombre_not + '&usuario_not=' + usuario_not + '&fecha_ingreso_not='+ fecha_ingreso_not + '&descripcion_not=' + descripcion_not + '&cabecera_not='+ cabecera_not + '&id_not=' + id_not;
	console.log(dataString);   
	$.ajax({
		url:'../servidor/gr_man_noticia.php',
		type:'POST',
		data: dataString,
		success:function(datos){

			alert("Noticia Modificada con Exito");
			location.href="man_noticia.php";
		}
	});
}


function eliminar(id_not)
{
	
	if(id_not==null){ 
		var id_not = $("#id_not").val(); 
	}
	var dataString = 'op=3&id_not='+ id_not;
	  swal({   
			title: "Eliminar Registro",   
			text: "Este registro no volverá a estar disponible.",   
			type: "warning",   
			showCancelButton: true,   
			confirmButtonColor: "#DD6B55",   
			confirmButtonText: "Si, Eliminar!",   
			closeOnConfirm: true,
			cancelButtonText: 'Cancelar'
		}, function(){  
			$.ajax({
					url:'../servidor/gr_man_noticia.php',
					type:'POST',
					data:dataString,
					success:function(datos){
						swal("Deleted!", "Your imaginary file has been deleted.", "success");
						//alert("Banco Eliminado");
						location.href="man_noticia.php";
					}
				}); 
		});
}

 function cargar_datos(id_not){
    var dataString = 'op=99&id_not='+ id_not;
	   $.ajax({
		url:'../servidor/gr_man_noticia.php',
		dataType:'json',
		type:'POST',
		data:dataString,
		success:function(datos){
			$('#id_not').val(datos[0].id_not);
			$('#nombre_not').val(datos[0].nombre_not);
			$('#fecha_ingreso_not').val(datos[0].fecha_ingreso_not);
			$('#usuario_not').val(datos[0].usuario_not);
			// $('#descripcion_not').val(datos[0].descripcion_not);
			$('#summernote').code(datos[0].descripcion_not);
			$('#cabecera_not').val(datos[0].cabecera_not);


			$('#btn_ingresar').parent().css('display', 'none');	
			$('#btn_modificar').show();
			$('#btn_eliminar').show();
			$(document.body).animate({
			    'scrollTop':   $('#content').offset().top
			}, 500);

		}
	});
}

 