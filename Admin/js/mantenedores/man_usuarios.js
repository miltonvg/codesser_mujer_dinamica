// JavaScript Document
function validar(valor)
{
	if($('#nombre_usu').val()=="")
	{
		alert("Debe ingresar el Nombre");
		$('#nombre_usu').focus();
		return false;
	}

	if($('#usuario_usu').val()=="")
	{
		alert("Debe ingresar un Nombre de Usuario");
		$('#usuario_usu').focus();
		return false;
	}
	if($('#clave_usu').val()=="")
	{
		alert("Debe ingresar una Contraseña");
		$('#clave_usu').focus();
		return false;
	}

	if(valor==1) ingresar();
	if(valor==2) modificar();
	
}

function ingresar()
{
	var usuario_usu = $('#usuario_usu').val();
	var nombre_usu = $('#nombre_usu').val();
	var clave_usu = $('#clave_usu').val();
	var id_car = $('#id_car').val();

	var dataString = 'op=1&usuario_usu='+ usuario_usu + '&clave_usu=' + clave_usu + '&nombre_usu='+ nombre_usu + '&id_car=' + id_car;
	   $.ajax({
			url:'../servidor/gr_man_usuarios.php',
			type:'POST',
			data:dataString,
			success:function(datos){
				alert("Usuario Ingresado  con Exito");
				location.href="man_usuarios.php";
			}
		});
}
function modificar()
{
	var id_usu = $('#id_usu').val();
	var usuario_usu = $('#usuario_usu').val();
	var nombre_usu = $('#nombre_usu').val();
	var clave_usu = $('#clave_usu').val();
	var id_car = $('#id_car').val();

	var dataString = 'op=2&usuario_usu='+ usuario_usu + '&clave_usu=' + clave_usu + '&nombre_usu='+ nombre_usu + '&id_car=' + id_car + '&id_usu=' + id_usu;
	console.log(dataString);   
	$.ajax({
		url:'../servidor/gr_man_usuarios.php',
		type:'POST',
		data: dataString,
		success:function(datos){

			//alert("Usuario Modificado con Exito");
			//location.href="man_usuarios.php";
		}
	});
}


function eliminar(id_usu)
{
	
	if(id_usu==null){ 
		var id_usu = $("#id_usu").val(); 
	}
	var dataString = 'op=3&id_usu='+ id_usu;
	  swal({   
			title: "Eliminar Registro",   
			text: "Este registro no volverá a estar disponible.",   
			type: "warning",   
			showCancelButton: true,   
			confirmButtonColor: "#DD6B55",   
			confirmButtonText: "Si, Eliminar!",   
			closeOnConfirm: true,
			cancelButtonText: 'Cancelar'
		}, function(){  
			$.ajax({
					url:'../servidor/gr_man_usuarios.php',
					type:'POST',
					data:dataString,
					success:function(datos){
						swal("Deleted!", "Your imaginary file has been deleted.", "success");
						//alert("Banco Eliminado");
						location.href="man_usuarios.php";
					}
				}); 
			 
		});
}

 function cargar_datos(id_usu){
    var dataString = 'op=99&id_usu='+ id_usu;
	   $.ajax({
		url:'../servidor/gr_man_usuarios.php',
		dataType:'json',
		type:'POST',
		data:dataString,
		success:function(datos){
			$('#id_usu').val(datos[0].id_usu);
			$('#usuario_usu').val(datos[0].usuario_usu);
			$('#nombre_usu').val(datos[0].nombre_usu);
			$('#clave_usu').val(datos[0].clave_usu);
			$('#id_car').val(datos[0].id_car);

			$('#btn_ingresar').parent().css('display', 'none');	
			$('#btn_modificar').show();
			$('#btn_eliminar').show();
			$(document.body).animate({
			    'scrollTop':   $('#content').offset().top
			}, 500);

		}
	});
}

 