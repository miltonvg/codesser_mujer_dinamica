function validar(valor)
{

if($('#frm_evento').val()==0)
	{
		alert("Debe Ingresar el nombre del evento");
		$('#frm_evento').focus();
		return false;
	}
if($('#frm_descripcion').val()=="")
	{
		alert("Debe Ingresar una descripción al evento");
		$('#frm_descripcion').focus();
		return false;
	}

	if(valor==1) ingresar_nuevo();
	if(valor==2) modificar_evento();
	
}

function datos_evento(id_eve, fecha){
	event.preventDefault();
	var dataString = 'op=5'+'&id_eve='+id_eve+'&fecha='+fecha;
	$.ajax({
		url:'../servidor/gr_man_agenda.php',
		type:'POST',
		data: dataString,
		success:function(datos){
			$("#grilla_evento").show();
			$("#grilla_evento").html(datos);
		}
	});
}

function ingresar_nuevo()
{	

var hora_ingreso_eve = $("#frm_hora").val();
var nombre_eve= $("#frm_nombre").val();
var descripcion_eve= $("#frm_descripcion").val();
var fecha_ingreso_eve = $('#frm_fecha_ingreso_eve').val();
var dataString = 'op=1' +'&hora_ingreso_eve='+hora_ingreso_eve+'&nombre_eve=' + nombre_eve + '&descripcion_eve=' + descripcion_eve + '&fecha_ingreso_eve=' +fecha_ingreso_eve;
	   $.ajax({
		url:'../servidor/gr_man_agenda.php',
		type:'POST',
		data: dataString,
		success:function(datos){
			alert("Evento ingresado");
				var dataString = 'op=4'+'&fecha_dia='+fecha_ingreso_eve;
					event.preventDefault();
				   $.ajax({
					url:'../servidor/gr_man_agenda.php',
					type:'POST',
					data: dataString,
					success:function(datos){
						$("#grilla_evento").hide();
						$("#grilla_reserva").html(datos);
					}
				});
		}
	});
}

function modificar_evento()
{	

var id_eve = $("#frm_id_eve").val();
var hora_ingreso_eve = $("#frm_hora").val();
var nombre_eve= $("#frm_nombre").val();
var descripcion_eve= $("#frm_descripcion").val();
var fecha_ingreso_eve = $('#frm_fecha_ingreso_eve').val();
var dataString = 'op=2' +'&id_eve='+id_eve+'&hora_ingreso_eve='+hora_ingreso_eve+'&nombre_eve=' + nombre_eve + '&descripcion_eve=' + descripcion_eve + '&fecha_ingreso_eve=' +fecha_ingreso_eve;
	   event.preventDefault();
	   $.ajax({
		url:'../servidor/gr_man_agenda.php',
		type:'POST',
		data: dataString,
		success:function(datos){
			alert("Evento Modificado");
				var dataString = 'op=4'+'&fecha_dia='+fecha_ingreso_eve;
					event.preventDefault();
				   $.ajax({
					url:'../servidor/gr_man_agenda.php',
					type:'POST',
					data: dataString,
					success:function(datos){
						$("#grilla_evento").hide();
						$("#grilla_reserva").html(datos);
					}
				});
		}
	});
}


function eliminar_evento()
{	

var id_eve = $("#frm_id_eve").val();
var fecha = $('#frm_fecha_ingreso_eve').val();
var dataString = 'op=3' +'&id_eve='+id_eve;
	   event.preventDefault();
	   $.ajax({
		url:'../servidor/gr_man_agenda.php',
		type:'POST',
		data: dataString,
		success:function(datos){
			alert("Evento Eliminado");
				var dataString = 'op=4'+'&fecha_dia='+fecha;
					
				   $.ajax({
					url:'../servidor/gr_man_agenda.php',
					type:'POST',
					data: dataString,
					success:function(datos){
						$("#grilla_evento").hide();
						$("#grilla_reserva").html(datos);
					}
				});
		}
	});
}

function cancelar_evento()
{	
var fecha = $('#frm_fecha_ingreso_eve').val();
		var dataString = 'op=4'+'&fecha_dia='+fecha;
			
		   $.ajax({
			url:'../servidor/gr_man_agenda.php',
			type:'POST',
			data: dataString,
			success:function(datos){
				$("#grilla_evento").hide();
				$("#grilla_reserva").html(datos);
			}
		});
}


function buscar_cliente(pg, texto, id_disp_hora){
	    $.ajax({
		url:'buscar_cliente.php',
		type:'POST',
		data:'op=1&pg='+pg+'&texto='+texto+'&id_disp_hora='+ id_disp_hora,
		success:function(datos){
			$("#grilla_reserva").html(datos);
		}
	});				
}



function ver_reserva(id_disp_hora){
	    $.ajax({
		url:'gr_reserva.php',
		type:'POST',
		data:'op=2&id_disp_hora='+ id_disp_hora,
		success:function(datos){
			$.fancybox(datos);
		}
	});				
}
