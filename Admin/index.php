<?php
include("funciones/setup.php");
include("funciones/fecha.php");
conecta();
$fechahoy=date("Y-m-d");
session_start();

if(isset($_SESSION['sesion_usuario_startup'])){

}else{
  header("Location:login.html");
}
?>
<!DOCTYPE html>
 
<html lang="es">
 
<head>
<title>Panel Principal</title>
<meta name="viewport" content="width=device-width, initial-scale=2.0, user-scalable=no">
<meta charset="utf-8" />
<link rel="shortcut icon" href="../images/favicon.png" />
<!-- Esto se especifica para que google encuentre donde esta alojada nuestra pagina web adaptada para móvil -->
<link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.example.com/page-1" >
<!-- FIN -->

<script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="css/estilos_base.css">
<link rel="stylesheet" href="css/estilos_forms.css">
<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="css/ionicons/css/ionicons.min.css">
</head>

 
<body>
<div id="cabecera">
<div id="credenciales">
      <table width="100%" border="0" cellspacing="3" cellpadding="0">
        <tr>
          <td colspan="2" class="fuente_normal">Hola <?php echo $_SESSION['sesion_usuario_startup'];?></td>
        </tr>
        <tr align="center">
              <td><a href="cerrar_sesion.php"><div class="button-close" ><i style="font-size:25px; color:#DD9696" class="ion-log-out"></i></div></a></td>
              <td><a href="panel_principal.php"><div class="button-return" ><i style="font-size:25px; color:#DD9696" class="ion-arrow-left-a"></i></div></a></td>
            </tr>
            <tr>
            <td colspan="2"><span class="fuente_login">La Serena, <?php echo fecha_hoy();?>&nbsp;</span></td>
            </tr>
      </table>
    </div>
 </div>
 <section>
 <ul id="panel_principal">
 <?php 
	if($_SESSION['sesion_tipo_startup'] == 1)
{	
?>
 <li>
 <div id=block>Usuario</div>
 <a href="mantenedores/man_usuarios.php">
  <i style="font-size:6em; color:#DD9696" class="ion-ios7-personadd-outline"></i>
 </a>
</li>
 
 <li>
 <div id=block>cargo</div>
 <a href="mantenedores/man_cargo.php">
  <i style="font-size:6em; color:#DD9696" class="ion-ios7-briefcase-outline"></i></a>

 </li>
 
 <li>
 <div id=block>Agenda</div>
 <a href="mantenedores/man_agenda.php">
  <i style="font-size:6em; color:#DD9696" class="ion-ios7-calendar-outline"></i></a>

 </li>
 
 <!-- <li>
 <div id=block>Img Fondo</div>
 <a href="mantenedores/man_img_fondo.php">
  <i style="font-size:5em; color:#DD9696" class="ion-android-image"></i></a>

 </li> -->
 
 <?php 
 }
	 if($_SESSION['sesion_tipo_startup'] == 1 or $_SESSION['sesion_tipo_startup'] == 2 or $_SESSION['sesion_tipo_startup'] == 3 or $_SESSION['sesion_tipo_startup'] == 4){	
 ?>
 <li>
 <div id=block>noticias</div>
 <a href="mantenedores/man_noticia.php">
  <i style="font-size:6em; color:#DD9696" class="ion-ios7-paper-outline"></i></a>

 </li>
 
 
 <?php }
	if($_SESSION['sesion_tipo_startup'] == 1)
{	
?>
 <!--<li id=block_informe>
 <div id=informe>Clientes Fav.</div>
 <a href="inf_cliente.php">
   <i style="font-size:6em; color:#999" class="fa fa-file-text-o"></i></a>

 </li>-->

 <?php } ?>
 </ul>
 </section>
</body>
</html>
